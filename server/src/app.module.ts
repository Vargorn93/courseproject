import { Module } from '@nestjs/common';
import { AuthController } from './app.controller';
import { GoogleAuthService } from './auth/google-auth.service';

@Module({
  imports: [],
  controllers: [AuthController],
  providers: [GoogleAuthService],
})
export class AppModule {}
