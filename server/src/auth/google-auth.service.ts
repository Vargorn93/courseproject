import { Injectable } from '@nestjs/common';
import { google } from 'googleapis'

@Injectable()
export class GoogleAuthService {
  getRedirectUrl() {
    const oauth2Client = new google.auth.OAuth2(
      '982799803768-svpbd8scqcfi2clcjpj8u037djjnqs0i.apps.googleusercontent.com',
      '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',
      'http://localhost:4200'
    );
    
    const scopes = [
      'https://www.googleapis.com/auth/blogger',
      'https://www.googleapis.com/auth/calendar'
    ];
    
    return oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: scopes
    });
  }
}
