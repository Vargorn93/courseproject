import { Controller, Get } from '@nestjs/common';
import { GoogleAuthService } from './auth/google-auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly appService: GoogleAuthService) {}

  @Get('google')
  getHello(): string {
    return this.appService.getRedirectUrl();

    // return Buffer.from(url).toString('base64');
  }
}
