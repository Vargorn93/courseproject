import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Location } from '@angular/common'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private location: Location, private http: HttpClient) {

  }
  title = 'courseProject';
  async authenticateInGoogle() {
    const apiKey ='http://localhost:3000/'
    const url: string = await this.http.get(apiKey + 'auth/google', {responseType: 'text'}).toPromise();
    window.location.href = url;
  }
}
